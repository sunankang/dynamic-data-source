package work.jame.dynamic.aop;

import org.aopalliance.aop.Advice;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.ComposablePointcut;
import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.core.annotation.AnnotatedElementUtils;
import work.jame.dynamic.annonation.DataSource;


/**
 * @author : Jame
 * @date : 2023-11-25 14:17
 **/
public class DynamicDataSourceAnnotationAdvisor extends AbstractPointcutAdvisor {

    private final Advice advice;

    private final Pointcut pointcut;

    public DynamicDataSourceAnnotationAdvisor(Advice advice) {
        this.advice = advice;
        pointcut = buildPointcut();
    }


    @Override
    public Advice getAdvice() {
        return this.advice;
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }


    private Pointcut buildPointcut() {
        Pointcut cpc = new AnnotationMatchingPointcut(DataSource.class, true);
        Pointcut mpc = AnnotationMatchingPointcut.forMethodAnnotation(DataSource.class);
        return new ComposablePointcut(cpc).union(mpc);
    }
}
