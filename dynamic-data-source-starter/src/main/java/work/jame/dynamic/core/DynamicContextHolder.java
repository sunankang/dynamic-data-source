package work.jame.dynamic.core;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author : Jame
 * @date : 2023-11-24 08:38
 **/
public class DynamicContextHolder {

    private static final ThreadLocal<Deque<String>> CONTEXT_HOLDER = ThreadLocal.withInitial(ArrayDeque::new);

    public static void push(String dbName) {
        CONTEXT_HOLDER.get().push(dbName);
    }

    public static String peek() {
        return CONTEXT_HOLDER.get().peek();
    }


    public static void poll() {
        CONTEXT_HOLDER.get().poll();
        if (CONTEXT_HOLDER.get().isEmpty()) {
            CONTEXT_HOLDER.remove();
        }
    }
}
