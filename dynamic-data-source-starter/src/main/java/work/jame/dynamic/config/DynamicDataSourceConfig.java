package work.jame.dynamic.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import work.jame.dynamic.core.AutoHandler;

import java.util.Map;

/**
 * @author : Jame
 * @date : 2023-11-24 09:19
 **/
@Getter
@Setter
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component(value = "dynamicDataSourceConfig")
@AutoConfigureBefore(DataSourceAutoConfiguration.class)
@Import(AutoHandler.class)
@ConfigurationProperties(prefix = "dynamic")
public class DynamicDataSourceConfig  {

    private Map<String, DataSourceProperties> dataSources;

    private String defaultDataSourceKey="master";


}
