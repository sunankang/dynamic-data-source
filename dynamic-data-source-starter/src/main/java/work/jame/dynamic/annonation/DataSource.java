package work.jame.dynamic.annonation;

import java.lang.annotation.*;

/**
 * @author : Jame
 * @date : 2023-11-24 10:18
 **/
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataSource {

    String value();

}
